# Over Engineered Power Switch

Remote control the power switch of a computer. This is used as a lazy form of "IPMI" control for a server that doesn't have an IPMI port

My use case for this is managing some servers through an espressobin for the sake of power consumption. One has an IPMI port, the other doesn't, so I am using a relay to control the pwoer of the server.

**NOTE:** `switch.sh` is hardcoded to use `/dev/ttyUSB0` so update that accordingly if it's using a different USB slot

## Arduino install and setup

* Open `relay.ino` in arduino IDE
* Upload file to arduino (in my case, an arduino nano)
* Wire relay to 5v, GND, and D3

## Remote machine setup

* Copy `switch.sh` to the machine
* Plug in arduino