// constants won't change. Used here to set a pin number:
const int relayPin =  3;// the number of the LED pin

// Variables will change:
int relayState = HIGH;             // relayState used to set the Relay
String command;

void setup() {
  // set the digital pin as output:
  pinMode(3, OUTPUT);
  Serial.begin(9600);
  Serial.println("Arduino Power Switch");
}

void loop() {
  digitalWrite(relayPin, relayState);
  if(Serial.available()){
      command = Serial.readStringUntil('\n');

      if (command == "on") {
        triggerRelay(1);
      } else if (command == "off") {
        triggerRelay(5);
      }
  }
}

void triggerRelay(int interval) {
  Serial.println("Engaging relay");
  relayState = LOW;
  digitalWrite(relayPin, relayState);
  delay(10*60*interval);
  Serial.println("Disengaging relay");
  relayState = HIGH;
  digitalWrite(relayPin, relayState);
}
