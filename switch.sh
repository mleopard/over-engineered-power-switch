#!/bin/bash

# Over engineered computer power switch controller
#
# Options:
# on - short 1 second burst to turn on linked computer
# off - 5 second burst to turn off linked computer
#

sudo su -c "echo $1 > /dev/ttyUSB0"
